# TP Apache Storm

## Introduction

Ce TP a pour but de pratiquer le codage de topologies dans Apache Storm.
Il n'est pas à rendre.

# Prise en main d'Apache STORM

## Brefs rappels sur Apache STORM

Il vous est rappelé ci-dessous quelques éléments/concepts importants
pour comprendre le fonctionnement d'une application reposant sur Apache
STORM.

La requête continue que vous souhaitez mettre en place pour interroger
un flux est représentée par une **topologie**. Cette topologie est un
*workflow* d'opérateurs qui vont s'enchaîner pour générer le flux de
sortie de votre topologie. Ces opérateurs sont appelés **spout** quand
ils servent de connecteur aux flux d'entrée et **bolt** quand ils
servent à implémenter un traitement. La coordination de la ou des
topologies se fait grâce à un **nimbus** qu'il faut voir comme un
*master*. Ce nimbus gère les allocations des opérateurs sur les nœuds de
traitements appelés **supervisors**. Pour gérer la coordination, le
master a besoin de monitorer ce qui se passe au niveau des supervisors,
pour cela il exploite les informations issues de **zookeeper**. Vous, en
tant qu'utilisateur/développeur, vous avez besoin de visualiser
l'activité de votre application, pour cela, Apache STORM propose une
interface web appelée **ui**.

Ainsi, pour lancer une application Apache STORM, vous avez besoin de
lancer :

  - un **zookeper** pour le monitoring
  - un **nimbus** pour la coordination
  - un **ui** pour la visualisation
  - un **supervisor** pour l'execution
  - une **topologie** comprenant un ou plusieurs **spouts** et **bolts**
    qui exprime une requête

## Environnement

### Préparer sa machine

Installer [Apache Storm](http://storm.apache.org/): extraire
[l'archive](https://perso.liris.cnrs.fr/ecoquery/files/apache-storm-1.2.2.tar.gz)
et ajouter si vous le souhaitez le répertoire `bin` dans votre `PATH`.

> Sur une machine de TP, travaillez de préférences sous Linux.
> Les logiciels peuvent être installés dans `/tmp/pxyz` o``pxyz`est votre login étudiants

> Sur une machine PC portable Windows, installer au besoin VirtualBox et lancer la machine virtuelle fournie (un disque dur et une clé USB vont circuler).

#### Installation Python

Le TP peut s'effectuer en Java ou en Python. 

Dans le cas Python, s'assurer que le package streamparse est installé:
```
pip install streamparse
pip3 install streamparse
```

Dans tous les cas il faudra Python >= 3.6 d'installé sur la machine (pour le moniteur, voir plus loin).

## Mise en place d'une topologie

Une topologie de départ est mise à disposition pour commencer. Elle est
composée de 3 opérateurs:

  - *TortoiseGeneratorSpout* qui génère des messages
  - *NothingBolt* qui ne fait que transmettre le message reçu à
    l'opérateur suivant.
  - *ExitBold* qui émet un message à destination de l'application de
    monitoring de la sortie.

### En Java

Dans 4 (onglets de) terminaux différents, lancer, depuis le répertoire d'installation de storm, les 4 commandes suivantes (dans cet ordre):
``` shell
bin/storm dev-zookeeper
bin/storm nimbus
bin/storm supervisor
bin/storm ui
```

Le projet Java peut être construit via Maven. La construction génère un
fichier `stormTP-2018-jar-with-dependencies.jar`. 


``` shell
bin/storm jar /chemin/vers/stormTP-0.1-jar-with-dependencies.jar stormTP.topology.TopologyT1
```

### En Python

L'exécution d'une topologie en python peut se faire en utilisant
[streamparse](https://github.com/Parsely/streamparse). Il suffit de
lancer la commande suivante depuis le répertoire `python/bdastorm`:

``` shell
sparse run -n topologyt1 -e local
```

A condition de démarrer les serveurs comme indiqué dans la section Java, il est aussi possible de lancer une topologie sur les serveur ainsi déployés via:

``` shell
sparse submit -n topologyt1 -e local
```

### Monitoring

Pour vous assurer que tout se passe correctement, vous disposez de deux
moniteurs.

Pour pouvoir visualiser les tuples en sortie de votre topologie,
exécuter le script python `stormmon.py`:

``` example
./monitor/stormmon.py
```

Pour visualiser le trafic dans votre topologie, vous avez l'interface
*ui* comme représenté sur les copies d'écran (seulement si vous utilisez les serveurs storm).

![Storm UI: Accueil](./images/stormui.png "stormui")

Sur la page d'accueil et dans la section "Topology summary", vous devez
avoir *topoT1* qui apparaît. En cliquant sur *topoT1*, vous accédez à
l'interface dédiée à cette topologie représentée sur la Figure 3. Les
deux boutons importants dans cette interface, sont les boutons:

  - **Kill** : qui vous permet d'arrêter votre topologie.
  - **Show visualization** : qui permet de visualiser graphiquement
    votre topologie.

![Storm UI: Topologie](./images/stormui2.png "topo")

### Tuer une topologie

Pour tuer une topologie:

- si vous avez lancé la topologie via `storm` ou `sparse submit`, vous pouvez utiliser le bouton kill dans l'interface
- si vous avez lancé la topologie via `sparse run`, il suffit de l'arrêter dans la console (`^C`).


# Course des tortues

Pour cette partie du TP, nous considérons une pseudo piste d'athlétisme.
Cette piste est composée de couloirs fragmentés en cellule. Chaque
couloir est composée de 254 cellules numérotées. Cette piste est le
terrain d'une course des tortues. Une tortue ne peut que rester sur la
cellule où elle se trouve ou avancer sur la cellule suivante.
L'évolution de la course est décrite par un flux de données dont le
schéma est (id, top, position, nbAvant, nbApres, nbTotal) avec *id* un
entier correspondant au dossard de la tortue qui l'identifie, *top* un
entier qui indique le numéro d'observation des tortues sur la piste,
*position* un entier qui correspond à la cellule courante où se trouve
la tortue (Attention, la position ne permet pas de déterminer le
classement de la tortue, car la piste est circulaire et qu'une tortue
peut avoir au moins un tour d'avance), *nbDevant* un entier qui indique
le nombre de tortues se trouvant devant la tortue dans le classement,
*nbDerriere* un entier qui indique le nombre de tortues se trouvant
derrières dans le classement et *total* indique le nombre total de
tortues en piste.

Remarque importante: pour ne pas gaspiller les ressources (CPU, RAM) de
votre machine, et sauf contre indication, il sera important de stopper
les topologies précédentes avant de lancer une nouvelle topologie (cf
bouton KILL sur l'interface UI de STORM)\[1\].

## Filtrer sa tortue

Il s'agit ici d'implémenter un opérateur *stateless*.

Définir un bolt, nommé "MyTortoiseBolt" qui récupère dans le flux la
tortue qui vous a été attribuée en début de séance. Les tuples retournés
par ce bolt ont pour schéma (id, top, nom, position, nbAvant, nbApres,
nbTotal). Ce qui correspond au schéma en entrée augmenté du nom de la
tortue (correspondant à nomBinôme1-nomBinôme2) qui vous a été attribuée.

Par exemple, si vous avez le dossard 1 et que vous êtes le binôme 'Yves
Atrovite' et 'Ella Paltan', à partir de l'objet JSON reçu:

``` json
{ "tortoises":[
  {"id":0,"top":523,"position":4,"nbDevant":8,"nbDerriere":1,"total":10},
  {"id":1,"top":523,"position":11,"nbDevant":4,"nbDerriere":4,"total":10},
  {"id":2,"top":523,"position":15,"nbDevant":1,"nbDerriere":7,"total":10},
  {"id":3,"top":523,"position":5,"nbDevant":7,"nbDerriere":2,"total":10},
  {"id":4,"top":523,"position":11,"nbDevant":4,"nbDerriere":4,"total":10},
  {"id":5,"top":523,"position":14,"nbDevant":3,"nbDerriere":6,"total":10},
  {"id":6,"top":523,"position":248,"nbDevant":0,"nbDerriere":9,"total":10},
  {"id":7,"top":523,"position":8,"nbDevant":6,"nbDerriere":3,"total":10},
  {"id":8,"top":523,"position":15,"nbDevant":1,"nbDerriere":7,"total":10},
  {"id":9,"top":523,"position":1,"nbDevant":9,"nbDerriere":0,"total":10}
] }
```

vous devez produire l'objet JSON:

``` json
{"id":1,"top":523, "nom":"Paltan-Atrovite",
 "position":11, "nbDevant":4, "nbDerriere":4, "total":10}   
```

A partir de *ExitBolt*, créer le bolt *Exit2Bolt* qui prend en entrée
des tuples de schéma (id, top, nom, position, nbDevant,
nbDerriere,total) et qui produit en sortie un tuple de schéma (json)
dont la valeur retournée correspond l'objet JSON attendu.

Définir la topologie TopologyT2, qui permettra de tester votre bolt
"MyTortoiseBolt" avec *TortoiseGeneratorSpout* et *Exit2Bolt*.

Tester votre topologie TopologyT2 (après avoir arrêté votre topologie
TopologyT1).

## Calcul du rang

Il s'agit ici d'implémenter un opérateur *stateless*.

Définir un bolt, nommé "GiveRankBolt" qui détermine le classement de
votre tortue sur la piste. Les tuples retournés par ce bolt ont pour
schéma (id, top, nom, rang, nbTotal). L'*id* correspond à l'identifiant
de votre tortue, le *top* correspond au top d'observation de votre
tortue, le *nom* correspond au nom de votre tortue et le *rang*
correspond à la chaîne de caractère indiquant le rang de la tortue. En
cas d'égalité, le rang des tortues *ex æquo* sera suffixé par le mot
'ex'.

A partir de *ExitBolt*, créer le bolt *Exit3Bolt* qui prend en entrée
des tuples de schéma (id, top, nom, rang, nbTotal) et qui produit en
sortie un tuple de schéma (json) dont la valeur retournée correspond
l'objet JSON attendu.

Ainsi pour le tuple reçu (1, 5, 'Toto',2 ,0 ,9 ,10), vous devrez générer
un tuple (1, 5, 'Toto', '1', 10). Pour le tuple reçu (1, 10, 'Toto', 6,
2, 6, 10), vous devrez générer un tuple (1, 10, 'Toto', '3ex', 10).

Définir la topologie TopologyT3, qui permettra de tester votre bolt
"GiveRankBolt" avec *TortoiseGeneratorSpout*, *MyTortoiseBolt* et
*Exit3Bolt*.

Tester votre topologie TopologyT3 (après avoir arrêté votre topologie
TopologyT2).

## Affectation des points bonus

Il s'agit ici d'implémenter un opérateur *stateful*.

Définir un bolt, nommé "ComputeBonusBolt" qui calcule le nombre de
points bonus cumulés par votre tortue. Les tuples retournés par ce bolt
ont pour schéma (id, top, nom, score). L'affectation des points bonus se
fait le de la manière suivante: tous les 15 tops, le classement de la
tortues est transformé en point correspondant au nombre total de
participants moins le rang dans le classement. Ainsi, pour 10
participants, le ou les premiers auront 9 points supplémentaires, le ou
les seconds auront 8 points supplémentaires et ainsi de suite.

A partir de *ExitBolt*, créer le bolt *Exit4Bolt* qui prend en entrée
des tuples de schéma (id, top, nom, points) et qui produit en sortie un
tuple de schéma (json) dont la valeur retournée correspond l'objet JSON
attendu.

Ainsi pour les tuples reçus {(1, 15, 'Toto', '1', 10), (1, 30, 'Toto',
'3ex', 10), (1, 45, 'Toto', '2', 10), (1, 60, 'Toto', '3', 10)} vous
devrez générer les tuples : (1, 15, 'Toto', 9), (1, 30, 'Toto', 15), (1,
45, 'Toto', 23) puis (1, 60, 'Toto', 30).

Définir la topologie TopologyT4, qui permettra de tester votre bolt
"ComputeBonusBolt" avec *TortoiseGeneratorSpout*, *MyTortoiseBolt*,
*GiveRankBolt* et *Exit4Bolt*.

Tester votre topologie TopologyT4 (après avoir arrêté votre topologie
TopologyT3).

## Vitesse moyenne

Il s'agit ici d'implémenter un opérateur *stateless* avec fenêtrage.

Définir un bolt, nommé "SpeedBolt, qui détermine la vitesse moyenne de
la tortue exprimée en cellule par top calculée sur 10 tops et ce, tous
les 5 tuples reçus. Si vous codez en Python, calculer plutôt la
vitesse moyenne sur les 10 dernières seconde. La bibliothèque
`pystorm` (utilisée par `streamparse`) ne permet pas de gérer
facilement des fenêtres glissantes. Les tuples retournés par ce bolt
ont pour schéma (id, nom, tops, vitesse), avec *tops* une chaîne de
caractères de la forme "t_i-t_i+9" où t_i et t_i+9 correspondent
respectivement au premier et au dernier top considérés dans le calcul.

Ainsi pour les tuples reçus { (1,1,'Toto',1,0,9,10);
(1,2,'Toto',1,0,9,10); (1,3,'Toto',1,0,9,10); (1,4,'Toto',2,0,9,10);
(1,5,'Toto',3,0,9,10); (1,6,'Toto',3,0,9,10); (1,7,'Toto',3,0,9,10);
(1,8,'Toto',4,0,9,10); (1,9,'Toto',4,0,9,10); (1,10,'Toto',4,0,9,10);
(1,11,'Toto',4,0,9,10); (1,12,'Toto',5,0,9,10); (1,13,'Toto',5,0,9,10);
(1,14,'Toto',5,0,9,10); (1,15,'Toto',6,0,9,10); }, vous devez générer
les tuples (1, 'Toto', '1-10', 0.40) puis (1, 'Toto', '6-15', 0.3) ;

A partir de *ExitBolt*, créer le bolt *Exit5Bolt* qui prend en entrée
des tuples de schéma (id, top, nom, vitesse) et qui produit en sortie un
tuple de schéma (json) dont la valeur retournée correspond l'objet JSON
attendu.

Définir la topologie TopologyT5, qui permettra de tester votre bolt
"SpeedBolt" avec *TortoiseGeneratorSpout* et *Exit5Bolt*.

Tester votre topologie TopologyT5 (après avoir arrêté votre topologie
TopologyT4).

## Évolution du rang

Il s'agit ici d'implémenter un opérateur *stateful* avec fenêtrage.

Définir un bolt, nommé "RankEvolutionBolt", qui détermine l'évolution du
rang ("En progression", "Constant" ou "En régression") de la tortue sur
une fenêtre de 10 secondes. Les tuples retournés par ce bolt ont pour
schéma (id, nom, tops, evolution). L'*id* correspond à l'identifiant de
la tortue, le *nom* correspond au nom de votre tortue, le *tops*
correspond à la concaténation du plus petit top observé dans la fenêtre
avec le plus grand top observé dans la même fenêtre et le *evolution*
correspond à la chaîne de caractères indiquant si la tortue est :

  - en progression, pour traduire qu'il a gagné au moins une place dans
    le classement au bout de 30 secondes
  - constant, pour traduire qu'il a la même place au bout de 30 secondes
  - en régression, pour traduire qu'il a perdu au moins une place dans
    le classement au bout de 30 secondes.

Ainsi pour les tuples reçus { (1,1,'Toto','1ex',10);
(1,2,'Toto','1',10); (1,3,'Toto','1',10); (1,4,'Toto','2',10);
(1,5,'Toto','1',10); (1,6,'Toto','1',10) } pendant 30 secondes, vous
devez générer un tuple (1, 'Toto', '1-6', 'Constant');

A partir de *ExitBolt*, créer le bolt *Exit6Bolt* qui prend en entrée
des tuples de schéma (id, top, nom, points) et qui produit en sortie un
tuple de schéma (json) dont la valeur retournée correspond l'objet JSON
attendu.

Définir la topologie TopologyT6, qui permettra de tester votre bolt
"RankEvolutionBolt" avec *TortoiseGeneratorSpout*, *MyTortoiseBolt*,
*GiveRankBolt* et *Exit6Bolt*.

Tester votre topologie TopologyT6 (après avoir arrêté votre topologie
TopologyT5).


\=\>=\>=\>=\>=\>=\>F=\>=\>=\>=\>=\>=\>\  
\=\>=\>=\>=\>=\>I=\>=\>=\>=\>=\>=\>=\>=|=\>=\>=\>=\>FIN  
\=\>=\>N=\>=\>=\>=\>=\>=\>=\>=\>=\>=\>/

1.  Mettre 0 comme valeur dans le dialogue et valider pour arrêter la
    topologie immédiatement
